﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Popups;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assessment2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {


        public MainPage()
        {
            this.InitializeComponent();

            //declaring combobox 
            var subjects = new List<string>();
            subjects.Add("Time-Table");
            subjects.Add("Prices");
            subjects.Add("Location");
            subjects.Add("Age group");
            subjects.Add("Type of Training");
            subjects.Add("Promotion");
            subjects.Add("Job Vacancy");
            subjects.Add("Other");

            comboBox.ItemsSource = subjects;
        }
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //calling combobox value
            lblSubject.Text = $"{comboBox.SelectedItem}";

        }
        private async void btlSubmit_Click(object sender, RoutedEventArgs e)
        {

            //create dialog box 
            var dialog = new MessageDialog("FROM:  " + txtFrom.Text + "\n" + "TO:  " + txtTo.Text + "\n" + "SUBJECT:  " + comboBox.SelectedItem + "\n" + "MESSAGE:  " + txtMessage.Text);
            dialog.Title = "Confirm and Send Message";

            dialog.Commands.Add(new UICommand { Label = "SEND", Id = 1 });
            dialog.Commands.Add(new UICommand("CANCEL", CancelCommand, 2));
            dialog.Commands.Add(new UICommand("RESET", ResetCommand, 3));

            var res = await dialog.ShowAsync();

            if ((int)res.Id == 1)
            {
                //send message
                dialog = new MessageDialog("sending message!!");
                dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
                var enterOK = await dialog.ShowAsync();
                if ((int)enterOK.Id == 0)
                {
                    Application.Current.Exit();
                }

            }
        }

        void ResetCommand(IUICommand command)
        {
            //reset message
            txtFrom.Text = "";
            txtTo.Text = "";
            comboBox.SelectedItem = null;
            txtMessage.Text = "";
        }
        void CancelCommand(IUICommand command)
        {
            //cancel message

        }


    }
}

